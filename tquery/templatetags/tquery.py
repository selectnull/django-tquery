from django import template


register = template.Library()


@register.filter
def where(qs, arg):
    """ Filters a query set by arg in format field=value, field2=value2 """
    filter_args = {kv.split('=')[0]:kv.split('=')[1] for kv in [x.strip() for x in arg.split(',')]}
    return qs.filter(**filter_args)

@register.filter
def limit(qs, arg):
    """ Limits a query set by arg items """
    return qs[:int(arg)]

@register.filter
def order_by(qs, arg):
    """ Orders a query set """
    return qs.order_by(arg)

@register.filter
def is_true(qs, arg):
    """ Filters a query set for true fields """
    return qs.filter(**{arg: True})

@register.filter
def is_false(qs, arg):
    """ Filters a query set for false fields """
    return qs.filter(**{arg: False})

@register.filter
def make_int_list(val):
    """ Makes a list of integers from comma separated string of numbers """
    return [int(x.strip()) for x in val.split(',') if x]
